package cn.jastz.app.mini.enums;

/**
 * @author zhiwen
 */
public enum AppMiniType {
    wechat, alipay
}
